module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Gatsby Default Starter`,
  },
  plugins: [
	  `gatsby-theme-docz`,
	  `gatsby-plugin-react-helmet`
  ],
}
